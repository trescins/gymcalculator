package com.example.kamil.gymcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.InjectView;

public class AddMealActivity extends AppCompatActivity {

    @InjectView(R.id.editTextCalories)
    EditText editTextcalories;

    @InjectView(R.id.editTextCarbons)
    EditText editTextCarbons;

    @InjectView(R.id.editTextFats)
    EditText editTextFats;

    @InjectView(R.id.editTextName)
    EditText editTextName;

    @InjectView(R.id.editTextProteins)
    EditText editTextProteins;

    @InjectView(R.id.editTextWeight)
    EditText editTextWeight;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meal);
    }
}
