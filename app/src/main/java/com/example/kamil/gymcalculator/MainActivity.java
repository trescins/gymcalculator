package com.example.kamil.gymcalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @InjectView(R.id.buttonAddMeal)
    Button buttonAddMeal;

    @InjectView(R.id.buttonCaloriesCalculator)
    Button buttonCaloriesCalculator;

    @InjectView(R.id.buttonShowMeals)
    Button buttonShowMeals;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);
    }

    @OnClick(R.id.buttonAddMeal)
            public void enterAddMealActivity() {
        Intent intent = new Intent(this, AddMealActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonCaloriesCalculator)
    public void enterCaloriesCalculatorActivity(){
        Intent intent = new Intent(this, CaloriesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonShowMeals)
    public void enterMealsBaseActivity(){
        Intent intent = new Intent(this, MealsBaseActivity.class);
        startActivity(intent);
    }


}