package com.example.kamil.gymcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MealsBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meals_base);
    }
}
